from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.core.mail import send_mail
from django.db import connection

from datetime import datetime, timedelta, date, time

from .models import *
from .utils import *


# Create your views here.
def root_page(request, *args, **kwargs):
    context = {
        "numcat": DoctorCategory.objects.count(),
        "numdoc": Doctor.objects.count(),
        "numapp": Appointment.objects.count(),
    }
    return render(request, "root.html", context)


def register(request, *args, **kwargs):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()

            group = request.POST.get("userType", "")
            if group == "client":
                user.groups.add(Group.objects.get_or_create(name="Clients"))
                client = Client(user=user)
                client.save()
            else:
                new_group, created = Group.objects.get_or_create(name="Doctors")
                user.groups.add(new_group)
                user.is_staff = True
                doctor = Doctor(user=user)
                doctor.save()
            user.first_name = request.POST["firstname"]
            user.last_name = request.POST["lastname"]
            user.save()
            login(request, user)
            return redirect("/welcome/")
        else:
            return render(request=request,
                          template_name="registration/register.html",
                          context={"form": form})
    form = UserCreationForm
    return render(request=request,
                  template_name="registration/register.html",
                  context={"form": form})


def client_browse_view(request, *args, **kwargs):
    context = {
        "categories": DoctorCategory.objects.all()
    }
    return render(request, "browse.html", context)


@login_required
def my_appointments_view(request, *args, **kwargs):
    if request.user.is_superuser:
        return redirect("/admin")

    context = {}
    app_id_to_disable = request.GET.get('appointment-id')
    if is_doctor(request.user):
        # get appointments
        context['appointments'] = request.user.doctor.appointment_set.filter(is_active=True)
        # check get param
        if app_id_to_disable is not None:
            # disable via ORM
            app_to_disable = Appointment.objects.get(id=app_id_to_disable)
            app_to_disable.is_active = False
            app_to_disable.save()
            context['deactivated'] = True
            # send email
            send_mail(
                'Your appointment was cancelled by the doctor',
                'Dear %s, your appointment to %s was cancelled by the doctor.' % (app_to_disable.client.user.username, app_to_disable.doctor.user.username),
                'admin@appointment.hu',
                [request.user.email]
            )

    elif is_client(request.user):
        # get appoitments
        context['appointments'] = request.user.client.appointment_set.filter(is_active=True)
        # check get param
        if app_id_to_disable is not None:
            # disable via raw SQL
            with connection.cursor() as c:
                c.execute(
                    'UPDATE doctors_appointment SET is_active=0 WHERE id=%s AND client_id=%s' % (app_id_to_disable, request.user.client.id)
                )

                if c.rowcount:
                    context['deactivated'] = True
    return render(request, "my_appointments.html", context)


@login_required
def profile_view(request, *args, **kwargs):
    if request.method == 'POST':
        print(request.POST)
        if 'firstname' in request.POST and 'lastname' in request.POST:
            request.user.first_name = request.POST['firstname']
            request.user.last_name = request.POST['lastname']
            request.user.save()
        elif 'address' in request.POST:
            request.user.doctor.address = request.POST['address']
            request.user.save()
    context = {}
    return render(request, "profile.html", context)


def doctor_timetable_view(request, doctor_id, *args, **kwargs):
    if request.method == 'POST':
        if 'appointment-doctor' in request.POST and request.user.is_authenticated:
            # appointment registration received, accept if client, else reject:
            if is_client(request.user):
                this_date = my_date_from_iso(int(request.POST['appointment-week']),
                                             int(request.POST['appointment-weekday']) + 1)
                this_time = request.POST['appointment-time']
                this_time = time(int(this_time[0:2]), int(this_time[3:5]))
                new_appointment = Appointment(
                    doctor=Doctor.objects.get(id=request.POST['appointment-doctor']),
                    client=request.user.client,
                    datetime=make_aware(datetime.combine(this_date, this_time))
                )
                new_appointment.save()
                return redirect('/my_appointments')
            else:
                return render(request, 'timetable.html', {"authorization_exception": True})
        elif 'week' in request.POST:
            week = request.POST['week']

        else:
            return render(request, 'timetable.html', {"authorization_exception": True})
    else:
        week = datetime.now().isocalendar()[1]

    try:
        doctor = Doctor.objects.get(id=doctor_id)
    except Doctor.DoesNotExist:
        raise Http404
    context = {
        "doctor": doctor,
        "week": week,
        "timetable": calculate_timetable_week(week, doctor.schedule)
    }
    return render(request, "timetable.html", context)

@login_required
def welcome_view(request, *args, **kwargs):
    context = {}
    return render(request, "welcome.html", context)
