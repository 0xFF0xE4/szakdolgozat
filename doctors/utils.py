from datetime import datetime, date, time, timedelta
from django.utils.timezone import make_aware

from .models import Appointment


def calculate_timetable_week(week, schedule):
    '''
        creates a weekly timetable for a doctor which schedule it gets,
        fills it with free, busy or inactive status recording to schedule and active appointments to this doctor
        return a dictionary which has a time_windows as a key and a status as a value
    '''
    timetable = {}
    # 1. get schedule info
    days = [
        (schedule.monday_from, schedule.monday_to),
        (schedule.tuesday_from, schedule.tuesday_to),
        (schedule.wednesday_from, schedule.wednesday_to),
        (schedule.thursday_from, schedule.thursday_to),
        (schedule.friday_from, schedule.friday_to),
        (schedule.saturday_from, schedule.saturday_to)
    ]
    current_time = min([day[0] for day in days])
    time_end = max([day[1] for day in days])

    # 2. fill timetable with free and inactive status
    while current_time < time_end:
        timetable[current_time.strftime("%H:%M")] = []
        for day in days:
            timetable[current_time.strftime("%H:%M")].append('free' if day[0] <= current_time < day[1] else 'inactive')
        current_time = (datetime.combine(date.today(), current_time) + timedelta(seconds=schedule.interval * 60)).time()

    # 3. set busy status
    booked_appointments = Appointment.objects.all().filter(
        doctor=schedule.doctor,
        is_active=True,
        datetime__week=week,
    )
    for appointment in booked_appointments:
        app_time = appointment.datetime.time().strftime("%H:%M")
        weekday = appointment.datetime.weekday()
        # check for sunday, just for sure
        if weekday == 6:
            weekday = 5
        timetable[app_time][weekday] = 'busy'
    return timetable


def my_date_from_iso(week, weekday):
    '''
        this method was added in python3.8, for this project I used python 3.7, so I created it by myself
        it creates date object from iso format (year, week of the year, day of the week)
    '''
    proper_date = datetime.strptime('2020-%02d-%d' % (week, weekday), '%Y-%W-%w')
    if date(2020, 1, 4).isoweekday() > 4:
        proper_date -= timedelta(days=7)
    return make_aware(proper_date)


def is_doctor(user):
    return user.groups.filter(name='Doctors').exists()


def is_client(user):
    return user.groups.filter(name='Clients').exists()
