from django.db import models
from django.conf import settings

import datetime


class DoctorCategory(models.Model):
    name = models.CharField(max_length=127)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Doctor categories"


class Doctor(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    categories = models.ManyToManyField(DoctorCategory, blank=True)
    address = models.CharField(max_length=512, blank=True, default="")

    def __str__(self):
        return self.user.username


class Client(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class Appointment(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, null=True)
    client = models.ForeignKey(Client, on_delete=models.SET_NULL, null=True)
    datetime = models.DateTimeField()
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s: doctor %s, client %s" % (
            self.datetime.strftime("%Y-%m-%d"),
            self.doctor.user.username,
            self.client.user.username)

    class Meta:
        permissions = [("can_change_is_active", "Can (de)activate appointments")]


class Schedule(models.Model):
    doctor = models.OneToOneField(Doctor, on_delete=models.CASCADE)
    default_start = datetime.time(8, 0)
    default_end = datetime.time(16, 0)
    interval = models.PositiveSmallIntegerField(default=15)
    monday_from = models.TimeField(default=default_start)
    monday_to = models.TimeField(default=default_end)
    tuesday_from = models.TimeField(default=default_start)
    tuesday_to = models.TimeField(default=default_end)
    wednesday_from = models.TimeField(default=default_start)
    wednesday_to = models.TimeField(default=default_end)
    thursday_from = models.TimeField(default=default_start)
    thursday_to = models.TimeField(default=default_end)
    friday_from = models.TimeField(default=default_start)
    friday_to = models.TimeField(default=default_end)
    saturday_from = models.TimeField(default=default_start)
    saturday_to = models.TimeField(default=default_end)

    def __str__(self):
        return self.doctor.user.username
