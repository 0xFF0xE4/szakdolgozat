from django.test import TestCase
from django.contrib.auth.models import User
from django.utils.timezone import make_aware
from datetime import datetime, time

from doctors.views import *
from doctors.models import *
from doctors.utils import *

class TestRootView(TestCase):

	def test_available(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)


class TestRegisterView(TestCase):

	def test_available(self):
		response = self.client.get('/accounts/register')
		self.assertEqual(response.status_code, 200)

	def test_post_registration(self):
		response = self.client.post(
			'/accounts/register',
			{
				'username': 'testuser', 
				'password1': 'testpasswd', 
				'password2': 'testpasswd', 
				'email': 'test@appointment.hu', 
				'userType': 'doctor',
				'firstname': 'Test',
				'lastname': 'User'
			},
			Follow=True
		)
		# redirect to welcome 
		self.assertRedirects(response, '/welcome/')
		# user was created
		test_user = User.objects.get(username='testuser')
		# correct user was logged in
		self.assertEqual(int(self.client.session['_auth_user_id']), test_user.pk)


class TestLoginView(TestCase):

	def test_available(self):
		response = self.client.get('/accounts/login/')
		self.assertEqual(response.status_code, 200)

	def test_post_login(self):
		test_user = User.objects.create_user('testuser', 'testpasswd')
		test_user.set_password('testpasswd')
		doctor = Doctor.objects.create(user=test_user)
		test_user.save()
		doctor.save()
		response = self.client.post(
			'/accounts/login/',
			{
				'username': 'testuser',
				'password': 'testpasswd'				
			}
		)
		self.assertRedirects(response, '/my_appointments')


class TestBrowseView(TestCase):

	def test_available(self):
		response = self.client.get('/browse/')
		self.assertEqual(response.status_code, 200)


class TestTimetableView(TestCase):

	def test_available(self):
		doctor = Doctor.objects.create(user=User.objects.create_user('testuser', 'testpasswd'))
		Schedule.objects.create(doctor=doctor,interval=60)
		response = self.client.get('/doctor/1/')
		self.assertEqual(response.status_code, 200)

	def test_notfound(self):
		response = self.client.get('/doctor/1/')
		self.assertEqual(response.status_code, 404)


class TestMyAppointmentsView(TestCase):

	def test_unauthorized_redirect(self):
		# on get
		response = self.client.get('/my_appointments')
		self.assertRedirects(response, '/accounts/login/?next=/my_appointments')
		# on post
		response = self.client.post('/my_appointments', { 'appointment-id': 1 })
		self.assertRedirects(response, '/accounts/login/?next=/my_appointments')

	def test_available_logged_in(self):
		test_user = User.objects.create_user('testuser', 'testpasswd')
		Doctor.objects.create(user=test_user)
		self.client.force_login(test_user)
		response = self.client.get('/my_appointments')
		self.assertEqual(response.status_code, 200)

	def test_get_deactivate(self):
		# create data:
		user = User.objects.create_user('testuser', 'testpasswd')
		doctorGroup = Group.objects.create(name='Doctors')
		doctorGroup.user_set.add(user)

		other_user = User.objects.create_user('other', 'password')
		clientGroup = Group.objects.create(name='Clients')
		clientGroup.user_set.add(other_user)

		doctor = Doctor.objects.create(user=user)
		client = Client.objects.create(user=other_user)
		app = Appointment.objects.create(
			doctor=doctor, 
			client=client,
			datetime=make_aware(datetime.combine(
				my_date_from_iso(week=1, weekday=1), 
				time(8, 0))
			)
		)
		app.save()
		self.client.force_login(user)

		# send request
		response = self.client.get('/my_appointments', { 'appointment-id': app.pk })

		# check if inactive
		self.assertFalse(Appointment.objects.get(id=app.pk).is_active)



class TestProfileView(TestCase):

	def test_unauthorized_redirect(self):
		response = self.client.get('/profile/')
		self.assertRedirects(response, '/accounts/login/?next=/profile/')

	def test_available_logged_in(self):
		test_user = User.objects.create_user('testuser', 'testpasswd')
		Doctor.objects.create(user=test_user)
		self.client.force_login(test_user)
		response = self.client.get('/profile/')
		self.assertEqual(response.status_code, 200)


class TestWelcomeView(TestCase):

	def test_unauthorized_redirect(self):
		response = self.client.get('/welcome/')
		self.assertRedirects(response, '/accounts/login/?next=/welcome/')

	def test_available_logged_in(self):
		test_user = User.objects.create_user('testuser', 'testpasswd')
		Doctor.objects.create(user=test_user)
		self.client.force_login(test_user)
		response = self.client.get('/welcome/')
		self.assertEqual(response.status_code, 200)
