from django.test import TestCase
from django.test import tag

from django.contrib.auth.models import User, Group
from django.utils.timezone import make_aware

from doctors.views import *
from doctors.models import *
from doctors.utils import *


@tag('vuln')
class VulnerabilitiesTestCase(TestCase):

	@classmethod
	def setUpTestData(cls):
		# create two clients and one doctor
		doctorGroup = Group.objects.create(name='Doctors')
		clientGroup = Group.objects.create(name='Clients')

		client1 = User.objects.create_user('client1', 'testpasswd')
		client2 = User.objects.create_user('client2', 'testpasswd')
		doctor = User.objects.create_user('doctor', 'testpasswd')

		clientGroup.user_set.add(client1, client2)
		doctorGroup.user_set.add(doctor)

		Client.objects.create(user=client1)
		Client.objects.create(user=client2)
		
		doc = Doctor.objects.create(user=doctor)
		Schedule.objects.create(doctor=doc, interval=60)

	def test_make_appointment(self):
		print('\nTest case: making appointment on already booked window')
		# get clients and doctor:
		client1 = Client.objects.get(id=1)
		client2 = Client.objects.get(id=2)
		doctor = Doctor.objects.get(id=1)
		print('\tSetting an appointment for client1')
		dt = make_aware(datetime.combine(my_date_from_iso(week=5, weekday=5), time(9, 0)))
		Appointment.objects.create(doctor=doctor, client=client1, datetime=dt)

		print('\tLogging in as a client2')
		self.client.force_login(client2.user)

		print('\tSending a POST request using the id of client1\'s appointment')
		requestbody = {
			'appointment-week': '5',
			'appointment-weekday': '4', # weekdays counting from 0 here
			'appointment-time': '09:00',
			'appointment-doctor': '1'
		}
		response = self.client.post('/doctor/1/', requestbody)

		client2.refresh_from_db()

		print('\tAsserting that client2 has an appointment to the same doctor at the same time')
		self.assertTrue(client2.appointment_set.filter(doctor=doctor).filter(datetime=dt))

	def test_sql_injection(self):
		print('\nTest case: deactivating all appointments with SQL injection')
		# get a client and a doctor
		client = Client.objects.get(id=1)
		doctor = Doctor.objects.get(id=1)

		print('\tSetting three appointments for the client')
		for hour in range(8, 11):
			dt = make_aware(datetime.combine(my_date_from_iso(week=5, weekday=5), time(hour, 0)))
			Appointment.objects.create(doctor=doctor, client=client, datetime=dt)

		print('\tLogging in as a client')
		self.client.force_login(client.user)

		print('\tSending request containing injection')
		response = self.client.get('/my_appointments', {'appointment-id': '1 or 1=1;--'})

		print('\tCheck that all appointments got deactivated')
		self.assertEqual(Appointment.objects.filter(is_active=True).count(), 0)
