from django.test import TestCase
from django.contrib.auth.models import User, Group
from datetime import datetime, timedelta, date, time
from django.utils.timezone import make_aware

from doctors.utils import *
from doctors.models import Schedule, Doctor, Appointment, Client


class UtilsTest(TestCase):
	@classmethod
	def setUpTestData(cls):
		# create one doctor and one client
		user = User.objects.create_user('testuser', 'testpasswd')
		doctorGroup = Group.objects.create(name='Doctors')
		doctorGroup.user_set.add(user)

		other_user = User.objects.create_user('other', 'password')
		clientGroup = Group.objects.create(name='Clients')
		clientGroup.user_set.add(other_user)

		doctor = Doctor.objects.create(user=user)
		client = Client.objects.create(user=other_user)

		# create schedule for testing calculate_timetable_week 
		Schedule.objects.create(doctor=doctor, interval=60)

	def test_calculate_timetable_week(self):
		# count for first week 
		timetable = calculate_timetable_week(1, Schedule.objects.get(id=1))
		# given doctor doesn't have appointments, so there is no 'busy' value:
		self.assertFalse('busy' in timetable.values())
		# add an appointment and check again
		Appointment.objects.create(
			doctor=Doctor.objects.get(id=1), 
			client=Client.objects.get(id=1),
			datetime=make_aware(datetime.combine(
				my_date_from_iso(week=1, weekday=1), 
				time(8, 0))
			)
		)
		timetable = calculate_timetable_week(1, Schedule.objects.get(id=1))
		self.assertTrue(any('busy' in status for status in timetable.values()))

	def test_my_date_from_iso(self):
		# cheeck specific date
		test_date = make_aware(datetime(2020, 5, 5, 0, 0))
		converted_date = my_date_from_iso(19, 2) # I doublechecked this
		self.assertEqual(converted_date, test_date)

	def test_is_doctor(self):
		self.assertTrue(is_doctor(User.objects.get(username='testuser')))
		self.assertFalse(is_doctor(User.objects.get(username='other')))

	def test_is_client(self):
		self.assertTrue(is_client(User.objects.get(username='other')))
		self.assertFalse(is_client(User.objects.get(username='testuser')))