from django.contrib import admin
from django import forms
from django.conf import settings

from .models import Doctor, Client, Appointment, Schedule, DoctorCategory


# Register your models here.

@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    exclude = ('user',)
    filter_horizontal = ('categories',)

    def get_queryset(self, request):
        qs = super(DoctorAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(id=request.user.doctor.pk)


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    exclude = ('user',)

    def get_queryset(self, request):
        qs = super(ClientAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(id=request.user.client.pk)


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    fields = (('datetime', 'is_active'), ('client', 'doctor'))
    list_display = ('doctor', 'client', 'datetime', 'is_active', 'created_at')

    def get_queryset(self, request):
        qs = super(AppointmentAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(doctor_id=request.user.doctor.pk)


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):

    def get_fields(self, request, obj=None):
        fields = ('interval', ('monday_from', 'monday_to'),
                  ('tuesday_from', 'tuesday_to'),
                  ('wednesday_from', 'wednesday_to'),
                  ('thursday_from', 'thursday_to'),
                  ('friday_from', 'friday_to'),
                  ('saturday_from', 'saturday_to'))
        if request.user.is_superuser:
            fields += ('doctor',)
        return fields

    def get_queryset(self, request):
        qs = super(ScheduleAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(doctor_id=request.user.doctor.pk)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.doctor = request.user.doctor
        super().save_model(request, obj, form, change)


@admin.register(DoctorCategory)
class DoctorCategoryAdmin(admin.ModelAdmin):
    pass


admin.site.site_header = "APPointment CRUD Panel"
admin.site.site_title = "APPointment CRUD Panel"
admin.site.index_title = "Welcome to APPointment manage panel"
